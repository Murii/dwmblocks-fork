//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	//{"UPT:",	"/usr/share/i3blocks/uptime",	2,	1},
	{"HOME:",	"/usr/share/i3blocks/disk",	8,	2},
	{"HDD:",	"/usr/share/i3blocks/disk /dev/sdb2",	8,	3},
	//{"",  	    "/usr/share/i3blocks/gym",	16,	4},
	{"MEM:",	"/usr/share/i3blocks/memory",	2,	5},
	{"CPU:",	"/usr/share/i3blocks/cpu_usage",	1,	6},
	{"TEMP:",	"/usr/share/i3blocks/temperature",	4,	7},
	{"FAN:",	"/usr/share/i3blocks/fan",	2,	8},
	{"LOAD:",	"/usr/share/i3blocks/load_average",	1,	9},
	{"",	"/usr/share/i3blocks/wifi",	20,	10},
	{"",	"/usr/share/i3blocks/vpn",	5,	11},
	{"VOL:",	"/usr/share/i3blocks/volume",	1,	12},
	{"BAT:",	"/usr/share/i3blocks/battery",	12,	13},
	{"DATE:",	"/usr/share/i3blocks/date",	1,	14},
	{"",       "/usr/share/i3blocks/weather", 15, 15},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = "|";
